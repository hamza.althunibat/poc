#!/bin/sh

echo "Configuring NZ Org CA Settings"
echo "Enroll the CA admin"
sleep 2
mkdir -p $PWD/organizations/peerOrganizations/nz.fabric.org/

fabric-ca-client enroll -u https://admin:adminpw@ca.nz.fabric.org:7054 --caname ca-nz --tls.certfiles $PWD/organizations/fabric-ca/nz/tls-cert.pem

echo 'NodeOUs:
    Enable: true
    ClientOUIdentifier:
        Certificate: cacerts/ca-nz-fabric-org-7054-ca-nz.pem
        OrganizationalUnitIdentifier: client
    PeerOUIdentifier:
        Certificate: cacerts/ca-nz-fabric-org-7054-ca-nz.pem
        OrganizationalUnitIdentifier: peer
    AdminOUIdentifier:
        Certificate: cacerts/ca-nz-fabric-org-7054-ca-nz.pem
        OrganizationalUnitIdentifier: admin
    OrdererOUIdentifier:
        Certificate: cacerts/ca-nz-fabric-org-7054-ca-nz.pem
        OrganizationalUnitIdentifier: orderer' >$PWD/organizations/peerOrganizations/nz.fabric.org/msp/config.yaml

echo "Register peer0"
fabric-ca-client register --caname ca-nz --id.name peer0 --id.secret peer0pw --id.type peer --tls.certfiles $PWD/organizations/fabric-ca/nz/tls-cert.pem

echo "Register user"
fabric-ca-client register --caname ca-nz --id.name user1 --id.secret user1pw --id.type client --tls.certfiles $PWD/organizations/fabric-ca/nz/tls-cert.pem

echo "Register the org admin"
fabric-ca-client register --caname ca-nz --id.name nzAdmin --id.secret nzAdminpw --id.type admin --tls.certfiles $PWD/organizations/fabric-ca/nz/tls-cert.pem

mkdir -p $PWD/organizations/peerOrganizations/nz.fabric.org/peers

echo "Generate the peer0 msp"
mkdir -p $PWD/organizations/peerOrganizations/nz.fabric.org/peers/peer0.nz.fabric.org
fabric-ca-client enroll -u https://peer0:peer0pw@ca.nz.fabric.org:7054 --caname ca-nz -M $PWD/organizations/peerOrganizations/nz.fabric.org/peers/peer0.nz.fabric.org/msp --csr.hosts peer0.nz.fabric.org --tls.certfiles $PWD/organizations/fabric-ca/nz/tls-cert.pem

cp $PWD/organizations/peerOrganizations/nz.fabric.org/msp/config.yaml $PWD/organizations/peerOrganizations/nz.fabric.org/peers/peer0.nz.fabric.org/msp/config.yaml

echo "Generate the peer0-tls certificates"
fabric-ca-client enroll -u https://peer0:peer0pw@ca.nz.fabric.org:7054 --caname ca-nz -M $PWD/organizations/peerOrganizations/nz.fabric.org/peers/peer0.nz.fabric.org/tls --enrollment.profile tls --csr.hosts peer0.nz.fabric.org --csr.hosts localhost --tls.certfiles $PWD/organizations/fabric-ca/nz/tls-cert.pem

cp $PWD/organizations/peerOrganizations/nz.fabric.org/peers/peer0.nz.fabric.org/tls/tlscacerts/* $PWD/organizations/peerOrganizations/nz.fabric.org/peers/peer0.nz.fabric.org/tls/ca.crt
cp $PWD/organizations/peerOrganizations/nz.fabric.org/peers/peer0.nz.fabric.org/tls/signcerts/* $PWD/organizations/peerOrganizations/nz.fabric.org/peers/peer0.nz.fabric.org/tls/server.crt
cp $PWD/organizations/peerOrganizations/nz.fabric.org/peers/peer0.nz.fabric.org/tls/keystore/* $PWD/organizations/peerOrganizations/nz.fabric.org/peers/peer0.nz.fabric.org/tls/server.key

mkdir -p $PWD/organizations/peerOrganizations/nz.fabric.org/msp/tlscacerts
cp $PWD/organizations/peerOrganizations/nz.fabric.org/peers/peer0.nz.fabric.org/tls/tlscacerts/* $PWD/organizations/peerOrganizations/nz.fabric.org/msp/tlscacerts/ca.crt

mkdir -p $PWD/organizations/peerOrganizations/nz.fabric.org/tlsca
cp $PWD/organizations/peerOrganizations/nz.fabric.org/peers/peer0.nz.fabric.org/tls/tlscacerts/* $PWD/organizations/peerOrganizations/nz.fabric.org/tlsca/tlsca.nz.fabric.org-cert.pem

mkdir -p $PWD/organizations/peerOrganizations/nz.fabric.org/ca
cp $PWD/organizations/peerOrganizations/nz.fabric.org/peers/peer0.nz.fabric.org/msp/cacerts/* $PWD/organizations/peerOrganizations/nz.fabric.org/ca/ca.nz.fabric.org-cert.pem

mkdir -p $PWD/organizations/peerOrganizations/nz.fabric.org/users

echo "Generate the user msp"
mkdir -p $PWD/organizations/peerOrganizations/nz.fabric.org/users/User1@nz.fabric.org
fabric-ca-client enroll -u https://user1:user1pw@ca.nz.fabric.org:7054 --caname ca-nz -M $PWD/organizations/peerOrganizations/nz.fabric.org/users/User1@nz.fabric.org/msp --tls.certfiles $PWD/organizations/fabric-ca/nz/tls-cert.pem
cp $PWD/organizations/peerOrganizations/nz.fabric.org/msp/config.yaml $PWD/organizations/peerOrganizations/nz.fabric.org/users/User1@nz.fabric.org/msp/config.yaml

echo "Generate the org admin msp"
mkdir -p $PWD/organizations/peerOrganizations/nz.fabric.org/users/Admin@nz.fabric.org
fabric-ca-client enroll -u https://nzAdmin:nzAdminpw@ca.nz.fabric.org:7054 --caname ca-nz -M $PWD/organizations/peerOrganizations/nz.fabric.org/users/Admin@nz.fabric.org/msp --tls.certfiles $PWD/organizations/fabric-ca/nz/tls-cert.pem
cp $PWD/organizations/peerOrganizations/nz.fabric.org/msp/config.yaml $PWD/organizations/peerOrganizations/nz.fabric.org/users/Admin@nz.fabric.org/msp/config.yaml
