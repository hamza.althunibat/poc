#!/bin/sh

configtxgen -profile IrsChannel -outputCreateChannelTx $PWD/channel-artifacts/irschannel.tx -channelID irschannel

for orgmsp in DM DT DPW DC NZ; do

	echo "Generating anchor peer update transaction for ${orgmsp}"
	configtxgen -profile IrsChannel -outputAnchorPeersUpdate $PWD/channel-artifacts/${orgmsp}anchors.tx -channelID irschannel -asOrg ${orgmsp}

done
