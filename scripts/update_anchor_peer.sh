#!/bin/bash


updateDmPeer(){
    peer channel update -o orderer1.fabric.org:7050 -c irschannel -f $PWD/channel-artifacts/DManchors.tx --tls --cafile $ORDERER_CA
}

updateDtPeer(){
    peer channel update -o orderer1.fabric.org:7050 -c irschannel -f $PWD/channel-artifacts/DTanchors.tx --tls --cafile $ORDERER_CA
}

updateDpwPeer(){
    peer channel update -o orderer1.fabric.org:7050 -c irschannel -f $PWD/channel-artifacts/DPWanchors.tx --tls --cafile $ORDERER_CA
}

updateDcPeer(){
    peer channel update -o orderer1.fabric.org:7050 -c irschannel -f $PWD/channel-artifacts/DCanchors.tx --tls --cafile $ORDERER_CA
}

updateNzPeer(){
    peer channel update -o orderer1.fabric.org:7050 -c irschannel -f $PWD/channel-artifacts/NZanchors.tx --tls --cafile $ORDERER_CA
}

export -f updateDmPeer
export -f updateDtPeer
export -f updateDpwPeer
export -f updateDcPeer
export -f updateNzPeer
