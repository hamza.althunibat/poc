#!/bin/bash

echo 'updating all servers'
 ssh ubuntu@mgr 'sudo apt update && sudo apt full-upgrade -y && echo "done mgr"' 
 ssh ubuntu@orderer 'sudo apt update && sudo apt full-upgrade -y && echo "done orderer"' 
 ssh ubuntu@dm 'sudo apt update && sudo apt full-upgrade -y && echo "done dm"' 
 ssh ubuntu@nz 'sudo apt update && sudo apt full-upgrade -y && echo "done nz"' 
 ssh ubuntu@dt 'sudo apt update && sudo apt full-upgrade -y && echo "done dt"' 
 ssh ubuntu@dpw 'sudo apt update && sudo apt full-upgrade -y && echo "done dpw"' 
 ssh ubuntu@dc 'sudo apt update && sudo apt full-upgrade -y && echo "done dc"' 

 echo 'restarting servers'
 ssh ubuntu@mgr 'sudo reboot' 
 ssh ubuntu@orderer 'sudo reboot' 
 ssh ubuntu@dm 'sudo reboot' 
 ssh ubuntu@nz 'sudo reboot' 
 ssh ubuntu@dt 'sudo reboot' 
 ssh ubuntu@dpw 'sudo reboot' 
 ssh ubuntu@dc 'sudo reboot' 