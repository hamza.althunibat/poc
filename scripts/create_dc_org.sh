#!/bin/sh

echo "Configuring DC Org CA Settings"
echo "Enroll the CA admin"
sleep 2
mkdir -p $PWD/organizations/peerOrganizations/dc.fabric.org/

fabric-ca-client enroll -u https://admin:adminpw@ca.dc.fabric.org:7054 --caname ca-dc --tls.certfiles $PWD/organizations/fabric-ca/dc/tls-cert.pem

echo 'NodeOUs:
    Enable: true
    ClientOUIdentifier:
        Certificate: cacerts/ca-dc-fabric-org-7054-ca-dc.pem
        OrganizationalUnitIdentifier: client
    PeerOUIdentifier:
        Certificate: cacerts/ca-dc-fabric-org-7054-ca-dc.pem
        OrganizationalUnitIdentifier: peer
    AdminOUIdentifier:
        Certificate: cacerts/ca-dc-fabric-org-7054-ca-dc.pem
        OrganizationalUnitIdentifier: admin
    OrdererOUIdentifier:
        Certificate: cacerts/ca-dc-fabric-org-7054-ca-dc.pem
        OrganizationalUnitIdentifier: orderer' >$PWD/organizations/peerOrganizations/dc.fabric.org/msp/config.yaml

echo "Register peer0"
fabric-ca-client register --caname ca-dc --id.name peer0 --id.secret peer0pw --id.type peer --tls.certfiles $PWD/organizations/fabric-ca/dc/tls-cert.pem

echo "Register user"
fabric-ca-client register --caname ca-dc --id.name user1 --id.secret user1pw --id.type client --tls.certfiles $PWD/organizations/fabric-ca/dc/tls-cert.pem

echo "Register the org admin"
fabric-ca-client register --caname ca-dc --id.name dcAdmin --id.secret dcAdminpw --id.type admin --tls.certfiles $PWD/organizations/fabric-ca/dc/tls-cert.pem

mkdir -p $PWD/organizations/peerOrganizations/dc.fabric.org/peers

echo "Generate the peer0 msp"
mkdir -p $PWD/organizations/peerOrganizations/dc.fabric.org/peers/peer0.dc.fabric.org
fabric-ca-client enroll -u https://peer0:peer0pw@ca.dc.fabric.org:7054 --caname ca-dc -M $PWD/organizations/peerOrganizations/dc.fabric.org/peers/peer0.dc.fabric.org/msp --csr.hosts peer0.dc.fabric.org --tls.certfiles $PWD/organizations/fabric-ca/dc/tls-cert.pem

cp $PWD/organizations/peerOrganizations/dc.fabric.org/msp/config.yaml $PWD/organizations/peerOrganizations/dc.fabric.org/peers/peer0.dc.fabric.org/msp/config.yaml

echo "Generate the peer0-tls certificates"
fabric-ca-client enroll -u https://peer0:peer0pw@ca.dc.fabric.org:7054 --caname ca-dc -M $PWD/organizations/peerOrganizations/dc.fabric.org/peers/peer0.dc.fabric.org/tls --enrollment.profile tls --csr.hosts peer0.dc.fabric.org --csr.hosts localhost --tls.certfiles $PWD/organizations/fabric-ca/dc/tls-cert.pem

cp $PWD/organizations/peerOrganizations/dc.fabric.org/peers/peer0.dc.fabric.org/tls/tlscacerts/* $PWD/organizations/peerOrganizations/dc.fabric.org/peers/peer0.dc.fabric.org/tls/ca.crt
cp $PWD/organizations/peerOrganizations/dc.fabric.org/peers/peer0.dc.fabric.org/tls/signcerts/* $PWD/organizations/peerOrganizations/dc.fabric.org/peers/peer0.dc.fabric.org/tls/server.crt
cp $PWD/organizations/peerOrganizations/dc.fabric.org/peers/peer0.dc.fabric.org/tls/keystore/* $PWD/organizations/peerOrganizations/dc.fabric.org/peers/peer0.dc.fabric.org/tls/server.key

mkdir -p $PWD/organizations/peerOrganizations/dc.fabric.org/msp/tlscacerts
cp $PWD/organizations/peerOrganizations/dc.fabric.org/peers/peer0.dc.fabric.org/tls/tlscacerts/* $PWD/organizations/peerOrganizations/dc.fabric.org/msp/tlscacerts/ca.crt

mkdir -p $PWD/organizations/peerOrganizations/dc.fabric.org/tlsca
cp $PWD/organizations/peerOrganizations/dc.fabric.org/peers/peer0.dc.fabric.org/tls/tlscacerts/* $PWD/organizations/peerOrganizations/dc.fabric.org/tlsca/tlsca.dc.fabric.org-cert.pem

mkdir -p $PWD/organizations/peerOrganizations/dc.fabric.org/ca
cp $PWD/organizations/peerOrganizations/dc.fabric.org/peers/peer0.dc.fabric.org/msp/cacerts/* $PWD/organizations/peerOrganizations/dc.fabric.org/ca/ca.dc.fabric.org-cert.pem

mkdir -p $PWD/organizations/peerOrganizations/dc.fabric.org/users

echo "Generate the user msp"
mkdir -p $PWD/organizations/peerOrganizations/dc.fabric.org/users/User1@dc.fabric.org
fabric-ca-client enroll -u https://user1:user1pw@ca.dc.fabric.org:7054 --caname ca-dc -M $PWD/organizations/peerOrganizations/dc.fabric.org/users/User1@dc.fabric.org/msp --tls.certfiles $PWD/organizations/fabric-ca/dc/tls-cert.pem
cp $PWD/organizations/peerOrganizations/dc.fabric.org/msp/config.yaml $PWD/organizations/peerOrganizations/dc.fabric.org/users/User1@dc.fabric.org/msp/config.yaml

echo "Generate the org admin msp"
mkdir -p $PWD/organizations/peerOrganizations/dc.fabric.org/users/Admin@dc.fabric.org
fabric-ca-client enroll -u https://dcAdmin:dcAdminpw@ca.dc.fabric.org:7054 --caname ca-dc -M $PWD/organizations/peerOrganizations/dc.fabric.org/users/Admin@dc.fabric.org/msp --tls.certfiles $PWD/organizations/fabric-ca/dc/tls-cert.pem
cp $PWD/organizations/peerOrganizations/dc.fabric.org/msp/config.yaml $PWD/organizations/peerOrganizations/dc.fabric.org/users/Admin@dc.fabric.org/msp/config.yaml
