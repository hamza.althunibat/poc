#!/bin/sh

echo "Configuring DT Org CA Settings"
echo "Enroll the CA admin"
sleep 2
mkdir -p $PWD/organizations/peerOrganizations/dt.fabric.org/

fabric-ca-client enroll -u https://admin:adminpw@ca.dt.fabric.org:7054 --caname ca-dt --tls.certfiles $PWD/organizations/fabric-ca/dt/tls-cert.pem

echo 'NodeOUs:
    Enable: true
    ClientOUIdentifier:
        Certificate: cacerts/ca-dt-fabric-org-7054-ca-dt.pem
        OrganizationalUnitIdentifier: client
    PeerOUIdentifier:
        Certificate: cacerts/ca-dt-fabric-org-7054-ca-dt.pem
        OrganizationalUnitIdentifier: peer
    AdminOUIdentifier:
        Certificate: cacerts/ca-dt-fabric-org-7054-ca-dt.pem
        OrganizationalUnitIdentifier: admin
    OrdererOUIdentifier:
        Certificate: cacerts/ca-dt-fabric-org-7054-ca-dt.pem
        OrganizationalUnitIdentifier: orderer' >$PWD/organizations/peerOrganizations/dt.fabric.org/msp/config.yaml

echo "Register peer0"
fabric-ca-client register --caname ca-dt --id.name peer0 --id.secret peer0pw --id.type peer --tls.certfiles $PWD/organizations/fabric-ca/dt/tls-cert.pem

echo "Register user"
fabric-ca-client register --caname ca-dt --id.name user1 --id.secret user1pw --id.type client --tls.certfiles $PWD/organizations/fabric-ca/dt/tls-cert.pem

echo "Register the org admin"
fabric-ca-client register --caname ca-dt --id.name dtAdmin --id.secret dtAdminpw --id.type admin --tls.certfiles $PWD/organizations/fabric-ca/dt/tls-cert.pem

mkdir -p $PWD/organizations/peerOrganizations/dt.fabric.org/peers

echo "Generate the peer0 msp"
mkdir -p $PWD/organizations/peerOrganizations/dt.fabric.org/peers/peer0.dt.fabric.org
fabric-ca-client enroll -u https://peer0:peer0pw@ca.dt.fabric.org:7054 --caname ca-dt -M $PWD/organizations/peerOrganizations/dt.fabric.org/peers/peer0.dt.fabric.org/msp --csr.hosts peer0.dt.fabric.org --tls.certfiles $PWD/organizations/fabric-ca/dt/tls-cert.pem

cp $PWD/organizations/peerOrganizations/dt.fabric.org/msp/config.yaml $PWD/organizations/peerOrganizations/dt.fabric.org/peers/peer0.dt.fabric.org/msp/config.yaml

echo "Generate the peer0-tls certificates"
fabric-ca-client enroll -u https://peer0:peer0pw@ca.dt.fabric.org:7054 --caname ca-dt -M $PWD/organizations/peerOrganizations/dt.fabric.org/peers/peer0.dt.fabric.org/tls --enrollment.profile tls --csr.hosts peer0.dt.fabric.org --csr.hosts localhost --tls.certfiles $PWD/organizations/fabric-ca/dt/tls-cert.pem

cp $PWD/organizations/peerOrganizations/dt.fabric.org/peers/peer0.dt.fabric.org/tls/tlscacerts/* $PWD/organizations/peerOrganizations/dt.fabric.org/peers/peer0.dt.fabric.org/tls/ca.crt
cp $PWD/organizations/peerOrganizations/dt.fabric.org/peers/peer0.dt.fabric.org/tls/signcerts/* $PWD/organizations/peerOrganizations/dt.fabric.org/peers/peer0.dt.fabric.org/tls/server.crt
cp $PWD/organizations/peerOrganizations/dt.fabric.org/peers/peer0.dt.fabric.org/tls/keystore/* $PWD/organizations/peerOrganizations/dt.fabric.org/peers/peer0.dt.fabric.org/tls/server.key

mkdir -p $PWD/organizations/peerOrganizations/dt.fabric.org/msp/tlscacerts
cp $PWD/organizations/peerOrganizations/dt.fabric.org/peers/peer0.dt.fabric.org/tls/tlscacerts/* $PWD/organizations/peerOrganizations/dt.fabric.org/msp/tlscacerts/ca.crt

mkdir -p $PWD/organizations/peerOrganizations/dt.fabric.org/tlsca
cp $PWD/organizations/peerOrganizations/dt.fabric.org/peers/peer0.dt.fabric.org/tls/tlscacerts/* $PWD/organizations/peerOrganizations/dt.fabric.org/tlsca/tlsca.dt.fabric.org-cert.pem

mkdir -p $PWD/organizations/peerOrganizations/dt.fabric.org/ca
cp $PWD/organizations/peerOrganizations/dt.fabric.org/peers/peer0.dt.fabric.org/msp/cacerts/* $PWD/organizations/peerOrganizations/dt.fabric.org/ca/ca.dt.fabric.org-cert.pem

mkdir -p $PWD/organizations/peerOrganizations/dt.fabric.org/users

echo "Generate the user msp"
mkdir -p $PWD/organizations/peerOrganizations/dt.fabric.org/users/User1@dt.fabric.org
fabric-ca-client enroll -u https://user1:user1pw@ca.dt.fabric.org:7054 --caname ca-dt -M $PWD/organizations/peerOrganizations/dt.fabric.org/users/User1@dt.fabric.org/msp --tls.certfiles $PWD/organizations/fabric-ca/dt/tls-cert.pem
cp $PWD/organizations/peerOrganizations/dt.fabric.org/msp/config.yaml $PWD/organizations/peerOrganizations/dt.fabric.org/users/User1@dt.fabric.org/msp/config.yaml

echo "Generate the org admin msp"
mkdir -p $PWD/organizations/peerOrganizations/dt.fabric.org/users/Admin@dt.fabric.org
fabric-ca-client enroll -u https://dtAdmin:dtAdminpw@ca.dt.fabric.org:7054 --caname ca-dt -M $PWD/organizations/peerOrganizations/dt.fabric.org/users/Admin@dt.fabric.org/msp --tls.certfiles $PWD/organizations/fabric-ca/dt/tls-cert.pem
cp $PWD/organizations/peerOrganizations/dt.fabric.org/msp/config.yaml $PWD/organizations/peerOrganizations/dt.fabric.org/users/Admin@dt.fabric.org/msp/config.yaml
