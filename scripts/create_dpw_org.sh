#!/bin/sh

echo "Configuring DPW Org CA Settings"
echo "Enroll the CA admin"
sleep 2
mkdir -p $PWD/organizations/peerOrganizations/dpw.fabric.org/

fabric-ca-client enroll -u https://admin:adminpw@ca.dpw.fabric.org:7054 --caname ca-dpw --tls.certfiles $PWD/organizations/fabric-ca/dpw/tls-cert.pem

echo 'NodeOUs:
    Enable: true
    ClientOUIdentifier:
        Certificate: cacerts/ca-dpw-fabric-org-7054-ca-dpw.pem
        OrganizationalUnitIdentifier: client
    PeerOUIdentifier:
        Certificate: cacerts/ca-dpw-fabric-org-7054-ca-dpw.pem
        OrganizationalUnitIdentifier: peer
    AdminOUIdentifier:
        Certificate: cacerts/ca-dpw-fabric-org-7054-ca-dpw.pem
        OrganizationalUnitIdentifier: admin
    OrdererOUIdentifier:
        Certificate: cacerts/ca-dpw-fabric-org-7054-ca-dpw.pem
        OrganizationalUnitIdentifier: orderer' >$PWD/organizations/peerOrganizations/dpw.fabric.org/msp/config.yaml

echo "Register peer0"
fabric-ca-client register --caname ca-dpw --id.name peer0 --id.secret peer0pw --id.type peer --tls.certfiles $PWD/organizations/fabric-ca/dpw/tls-cert.pem

echo "Register user"
fabric-ca-client register --caname ca-dpw --id.name user1 --id.secret user1pw --id.type client --tls.certfiles $PWD/organizations/fabric-ca/dpw/tls-cert.pem

echo "Register the org admin"
fabric-ca-client register --caname ca-dpw --id.name dpwAdmin --id.secret dpwAdminpw --id.type admin --tls.certfiles $PWD/organizations/fabric-ca/dpw/tls-cert.pem

mkdir -p $PWD/organizations/peerOrganizations/dpw.fabric.org/peers

echo "Generate the peer0 msp"
mkdir -p $PWD/organizations/peerOrganizations/dpw.fabric.org/peers/peer0.dpw.fabric.org
fabric-ca-client enroll -u https://peer0:peer0pw@ca.dpw.fabric.org:7054 --caname ca-dpw -M $PWD/organizations/peerOrganizations/dpw.fabric.org/peers/peer0.dpw.fabric.org/msp --csr.hosts peer0.dpw.fabric.org --tls.certfiles $PWD/organizations/fabric-ca/dpw/tls-cert.pem

cp $PWD/organizations/peerOrganizations/dpw.fabric.org/msp/config.yaml $PWD/organizations/peerOrganizations/dpw.fabric.org/peers/peer0.dpw.fabric.org/msp/config.yaml

echo "Generate the peer0-tls certificates"
fabric-ca-client enroll -u https://peer0:peer0pw@ca.dpw.fabric.org:7054 --caname ca-dpw -M $PWD/organizations/peerOrganizations/dpw.fabric.org/peers/peer0.dpw.fabric.org/tls --enrollment.profile tls --csr.hosts peer0.dpw.fabric.org --csr.hosts localhost --tls.certfiles $PWD/organizations/fabric-ca/dpw/tls-cert.pem

cp $PWD/organizations/peerOrganizations/dpw.fabric.org/peers/peer0.dpw.fabric.org/tls/tlscacerts/* $PWD/organizations/peerOrganizations/dpw.fabric.org/peers/peer0.dpw.fabric.org/tls/ca.crt
cp $PWD/organizations/peerOrganizations/dpw.fabric.org/peers/peer0.dpw.fabric.org/tls/signcerts/* $PWD/organizations/peerOrganizations/dpw.fabric.org/peers/peer0.dpw.fabric.org/tls/server.crt
cp $PWD/organizations/peerOrganizations/dpw.fabric.org/peers/peer0.dpw.fabric.org/tls/keystore/* $PWD/organizations/peerOrganizations/dpw.fabric.org/peers/peer0.dpw.fabric.org/tls/server.key

mkdir -p $PWD/organizations/peerOrganizations/dpw.fabric.org/msp/tlscacerts
cp $PWD/organizations/peerOrganizations/dpw.fabric.org/peers/peer0.dpw.fabric.org/tls/tlscacerts/* $PWD/organizations/peerOrganizations/dpw.fabric.org/msp/tlscacerts/ca.crt

mkdir -p $PWD/organizations/peerOrganizations/dpw.fabric.org/tlsca
cp $PWD/organizations/peerOrganizations/dpw.fabric.org/peers/peer0.dpw.fabric.org/tls/tlscacerts/* $PWD/organizations/peerOrganizations/dpw.fabric.org/tlsca/tlsca.dpw.fabric.org-cert.pem

mkdir -p $PWD/organizations/peerOrganizations/dpw.fabric.org/ca
cp $PWD/organizations/peerOrganizations/dpw.fabric.org/peers/peer0.dpw.fabric.org/msp/cacerts/* $PWD/organizations/peerOrganizations/dpw.fabric.org/ca/ca.dpw.fabric.org-cert.pem

mkdir -p $PWD/organizations/peerOrganizations/dpw.fabric.org/users

echo "Generate the user msp"
mkdir -p $PWD/organizations/peerOrganizations/dpw.fabric.org/users/User1@dpw.fabric.org
fabric-ca-client enroll -u https://user1:user1pw@ca.dpw.fabric.org:7054 --caname ca-dpw -M $PWD/organizations/peerOrganizations/dpw.fabric.org/users/User1@dpw.fabric.org/msp --tls.certfiles $PWD/organizations/fabric-ca/dpw/tls-cert.pem
cp $PWD/organizations/peerOrganizations/dpw.fabric.org/msp/config.yaml $PWD/organizations/peerOrganizations/dpw.fabric.org/users/User1@dpw.fabric.org/msp/config.yaml

echo "Generate the org admin msp"
mkdir -p $PWD/organizations/peerOrganizations/dpw.fabric.org/users/Admin@dpw.fabric.org
fabric-ca-client enroll -u https://dpwAdmin:dpwAdminpw@ca.dpw.fabric.org:7054 --caname ca-dpw -M $PWD/organizations/peerOrganizations/dpw.fabric.org/users/Admin@dpw.fabric.org/msp --tls.certfiles $PWD/organizations/fabric-ca/dpw/tls-cert.pem
cp $PWD/organizations/peerOrganizations/dpw.fabric.org/msp/config.yaml $PWD/organizations/peerOrganizations/dpw.fabric.org/users/Admin@dpw.fabric.org/msp/config.yaml
