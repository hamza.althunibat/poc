#!/bin/bash

echo 'refreshing poc folder from all servers'
 ssh ubuntu@mgr 'cd poc && git fetch && git pull && echo "done mgr"' 
 ssh ubuntu@orderer 'cd poc && git fetch && git pull && echo "done orderer"' 
 ssh ubuntu@dm 'cd poc && git fetch && git pull && echo "done dm"' 
 ssh ubuntu@nz 'cd poc && git fetch && git pull && echo "done nz"' 
 ssh ubuntu@dt 'cd poc && git fetch && git pull && echo "done dt"' 
 ssh ubuntu@dpw 'cd poc && git fetch && git pull && echo "done dpw"' 
 ssh ubuntu@dc 'cd poc && git fetch && git pull && echo "done dc"' 
