#!/bin/bash

echo 'delete poc folder from all servers'
 ssh ubuntu@mgr 'sudo rm -rf poc && echo "done mgr"' 
 ssh ubuntu@orderer 'sudo rm -rf poc && echo "done orderer"' 
 ssh ubuntu@dm 'sudo rm -rf poc && echo "done dm"' 
 ssh ubuntu@nz 'sudo rm -rf poc && echo "done nz"' 
 ssh ubuntu@dt 'sudo rm -rf poc && echo "done dt"' 
 ssh ubuntu@dpw 'sudo rm -rf poc && echo "done dpw"' 
 ssh ubuntu@dc 'sudo rm -rf poc && echo "done dc"' 

 echo 'pull poc'
 ssh ubuntu@mgr 'git clone https://gitlab.com/hamza.althunibat/poc.git' 
 ssh ubuntu@orderer 'git clone https://gitlab.com/hamza.althunibat/poc.git' 
 ssh ubuntu@dm 'git clone https://gitlab.com/hamza.althunibat/poc.git' 
 ssh ubuntu@nz 'git clone https://gitlab.com/hamza.althunibat/poc.git' 
 ssh ubuntu@dt 'git clone https://gitlab.com/hamza.althunibat/poc.git' 
 ssh ubuntu@dpw 'git clone https://gitlab.com/hamza.althunibat/poc.git' 
 ssh ubuntu@dc 'git clone https://gitlab.com/hamza.althunibat/poc.git' 