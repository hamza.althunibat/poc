#!/bin/sh

echo "Configuring Orderer Org CA Settings"
echo "Enroll the CA Admin"
sleep 2
mkdir -p $PWD/organizations/ordererOrganizations/orderer.fabric.org
fabric-ca-client enroll -u https://admin:adminpw@ca.orderer.fabric.org:7054 --caname ca-orderer --tls.certfiles $PWD/organizations/fabric-ca/orderer/tls-cert.pem

echo 'NodeOUs:
    Enable: true
    ClientOUIdentifier:
        Certificate: cacerts/ca-orderer-fabric-org-7054-ca-orderer.pem
        OrganizationalUnitIdentifier: client
    PeerOUIdentifier:
        Certificate: cacerts/ca-orderer-fabric-org-7054-ca-orderer.pem
        OrganizationalUnitIdentifier: peer
    AdminOUIdentifier:
        Certificate: cacerts/ca-orderer-fabric-org-7054-ca-orderer.pem
        OrganizationalUnitIdentifier: admin
    OrdererOUIdentifier:
        Certificate: cacerts/ca-orderer-fabric-org-7054-ca-orderer.pem
        OrganizationalUnitIdentifier: orderer' >$PWD/organizations/ordererOrganizations/orderer.fabric.org/msp/config.yaml

echo "Register orderer1"
fabric-ca-client register --caname ca-orderer --id.name orderer1 --id.secret ordererpw --id.type orderer --tls.certfiles $PWD/organizations/fabric-ca/orderer/tls-cert.pem

echo "Register orderer2"
fabric-ca-client register --caname ca-orderer --id.name orderer2 --id.secret ordererpw --id.type orderer --tls.certfiles $PWD/organizations/fabric-ca/orderer/tls-cert.pem

echo "Register orderer3"
fabric-ca-client register --caname ca-orderer --id.name orderer3 --id.secret ordererpw --id.type orderer --tls.certfiles $PWD/organizations/fabric-ca/orderer/tls-cert.pem

echo "Register orderer4"
fabric-ca-client register --caname ca-orderer --id.name orderer4 --id.secret ordererpw --id.type orderer --tls.certfiles $PWD/organizations/fabric-ca/orderer/tls-cert.pem

echo "Register orderer5"
fabric-ca-client register --caname ca-orderer --id.name orderer5 --id.secret ordererpw --id.type orderer --tls.certfiles $PWD/organizations/fabric-ca/orderer/tls-cert.pem

echo "Register the orderer admin"
fabric-ca-client register --caname ca-orderer --id.name ordererAdmin --id.secret ordererAdminpw --id.type admin --tls.certfiles $PWD/organizations/fabric-ca/orderer/tls-cert.pem

mkdir -p $PWD/organizations/ordererOrganizations/orderer.fabric.org/msp/tlscacerts
mkdir -p $PWD/organizations/ordererOrganizations/orderer.fabric.org/orderers

mkdir -p $PWD/organizations/ordererOrganizations/orderer.fabric.org/users
mkdir -p $PWD/organizations/ordererOrganizations/orderer.fabric.org/users/Admin@orderer.fabric.org

echo "Generate the admin msp"
fabric-ca-client enroll -u https://ordererAdmin:ordererAdminpw@ca.orderer.fabric.org:7054 --caname ca-orderer -M $PWD/organizations/ordererOrganizations/orderer.fabric.org/users/Admin@orderer.fabric.org/msp --tls.certfiles $PWD/organizations/fabric-ca/orderer/tls-cert.pem
cp $PWD/organizations/ordererOrganizations/orderer.fabric.org/msp/config.yaml $PWD/organizations/ordererOrganizations/orderer.fabric.org/users/Admin@orderer.fabric.org/msp/config.yaml

# -----------------------------------------------------------------------
#  Orderer 1

echo "Preparing Orderer 1"

mkdir -p $PWD/organizations/ordererOrganizations/orderer.fabric.org/orderers/orderer1.fabric.org

echo "Generate the orderer msp"
fabric-ca-client enroll -u https://ordererAdmin:ordererAdminpw@ca.orderer.fabric.org:7054 --caname ca-orderer -M $PWD/organizations/ordererOrganizations/orderer.fabric.org/orderers/orderer1.fabric.org/msp --csr.hosts orderer1.fabric.org --csr.hosts localhost --tls.certfiles $PWD/organizations/fabric-ca/orderer/tls-cert.pem

cp $PWD/organizations/ordererOrganizations/orderer.fabric.org/msp/config.yaml $PWD/organizations/ordererOrganizations/orderer.fabric.org/orderers/orderer1.fabric.org/msp/config.yaml

echo "Generate the orderer-tls certificates"
fabric-ca-client enroll -u https://ordererAdmin:ordererAdminpw@ca.orderer.fabric.org:7054 --caname ca-orderer -M $PWD/organizations/ordererOrganizations/orderer.fabric.org/orderers/orderer1.fabric.org/tls --enrollment.profile tls --csr.hosts orderer1.fabric.org --csr.hosts localhost --tls.certfiles $PWD/organizations/fabric-ca/orderer/tls-cert.pem

cp $PWD/organizations/ordererOrganizations/orderer.fabric.org/orderers/orderer1.fabric.org/tls/tlscacerts/* $PWD/organizations/ordererOrganizations/orderer.fabric.org/orderers/orderer1.fabric.org/tls/ca.crt
cp $PWD/organizations/ordererOrganizations/orderer.fabric.org/orderers/orderer1.fabric.org/tls/signcerts/* $PWD/organizations/ordererOrganizations/orderer.fabric.org/orderers/orderer1.fabric.org/tls/server.crt
cp $PWD/organizations/ordererOrganizations/orderer.fabric.org/orderers/orderer1.fabric.org/tls/keystore/* $PWD/organizations/ordererOrganizations/orderer.fabric.org/orderers/orderer1.fabric.org/tls/server.key

mkdir -p $PWD/organizations/ordererOrganizations/orderer.fabric.org/orderers/orderer1.fabric.org/msp/tlscacerts
cp $PWD/organizations/ordererOrganizations/orderer.fabric.org/orderers/orderer1.fabric.org/tls/tlscacerts/* $PWD/organizations/ordererOrganizations/orderer.fabric.org/orderers/orderer1.fabric.org/msp/tlscacerts/tlsca.fabric.org-cert.pem

cp $PWD/organizations/ordererOrganizations/orderer.fabric.org/orderers/orderer1.fabric.org/tls/tlscacerts/* $PWD/organizations/ordererOrganizations/orderer.fabric.org/msp/tlscacerts/tlsca.fabric.org-cert.pem

# -----------------------------------------------------------------------
#  Orderer 2

echo "Preparing Orderer 2"

mkdir -p $PWD/organizations/ordererOrganizations/orderer.fabric.org/orderers/orderer2.fabric.org

echo "Generate the orderer msp"
fabric-ca-client enroll -u https://ordererAdmin:ordererAdminpw@ca.orderer.fabric.org:7054 --caname ca-orderer -M $PWD/organizations/ordererOrganizations/orderer.fabric.org/orderers/orderer2.fabric.org/msp --csr.hosts orderer2.fabric.org --csr.hosts localhost --tls.certfiles $PWD/organizations/fabric-ca/orderer/tls-cert.pem

cp $PWD/organizations/ordererOrganizations/orderer.fabric.org/msp/config.yaml $PWD/organizations/ordererOrganizations/orderer.fabric.org/orderers/orderer2.fabric.org/msp/config.yaml

echo "Generate the orderer-tls certificates"
fabric-ca-client enroll -u https://ordererAdmin:ordererAdminpw@ca.orderer.fabric.org:7054 --caname ca-orderer -M $PWD/organizations/ordererOrganizations/orderer.fabric.org/orderers/orderer2.fabric.org/tls --enrollment.profile tls --csr.hosts orderer2.fabric.org --csr.hosts localhost --tls.certfiles $PWD/organizations/fabric-ca/orderer/tls-cert.pem

cp $PWD/organizations/ordererOrganizations/orderer.fabric.org/orderers/orderer2.fabric.org/tls/tlscacerts/* $PWD/organizations/ordererOrganizations/orderer.fabric.org/orderers/orderer2.fabric.org/tls/ca.crt
cp $PWD/organizations/ordererOrganizations/orderer.fabric.org/orderers/orderer2.fabric.org/tls/signcerts/* $PWD/organizations/ordererOrganizations/orderer.fabric.org/orderers/orderer2.fabric.org/tls/server.crt
cp $PWD/organizations/ordererOrganizations/orderer.fabric.org/orderers/orderer2.fabric.org/tls/keystore/* $PWD/organizations/ordererOrganizations/orderer.fabric.org/orderers/orderer2.fabric.org/tls/server.key

mkdir -p $PWD/organizations/ordererOrganizations/orderer.fabric.org/orderers/orderer2.fabric.org/msp/tlscacerts
cp $PWD/organizations/ordererOrganizations/orderer.fabric.org/orderers/orderer2.fabric.org/tls/tlscacerts/* $PWD/organizations/ordererOrganizations/orderer.fabric.org/orderers/orderer2.fabric.org/msp/tlscacerts/tlsca.fabric.org-cert.pem

cp $PWD/organizations/ordererOrganizations/orderer.fabric.org/orderers/orderer2.fabric.org/tls/tlscacerts/* $PWD/organizations/ordererOrganizations/orderer.fabric.org/msp/tlscacerts/tlsca.fabric.org-cert.pem

# -----------------------------------------------------------------------
#  Orderer 3

echo "Preparing Orderer 3"

mkdir -p $PWD/organizations/ordererOrganizations/orderer.fabric.org/orderers/orderer3.fabric.org

echo "Generate the orderer msp"
fabric-ca-client enroll -u https://ordererAdmin:ordererAdminpw@ca.orderer.fabric.org:7054 --caname ca-orderer -M $PWD/organizations/ordererOrganizations/orderer.fabric.org/orderers/orderer3.fabric.org/msp --csr.hosts orderer3.fabric.org --csr.hosts localhost --tls.certfiles $PWD/organizations/fabric-ca/orderer/tls-cert.pem

cp $PWD/organizations/ordererOrganizations/orderer.fabric.org/msp/config.yaml $PWD/organizations/ordererOrganizations/orderer.fabric.org/orderers/orderer3.fabric.org/msp/config.yaml

echo "Generate the orderer-tls certificates"
fabric-ca-client enroll -u https://ordererAdmin:ordererAdminpw@ca.orderer.fabric.org:7054 --caname ca-orderer -M $PWD/organizations/ordererOrganizations/orderer.fabric.org/orderers/orderer3.fabric.org/tls --enrollment.profile tls --csr.hosts orderer3.fabric.org --csr.hosts localhost --tls.certfiles $PWD/organizations/fabric-ca/orderer/tls-cert.pem

cp $PWD/organizations/ordererOrganizations/orderer.fabric.org/orderers/orderer3.fabric.org/tls/tlscacerts/* $PWD/organizations/ordererOrganizations/orderer.fabric.org/orderers/orderer3.fabric.org/tls/ca.crt
cp $PWD/organizations/ordererOrganizations/orderer.fabric.org/orderers/orderer3.fabric.org/tls/signcerts/* $PWD/organizations/ordererOrganizations/orderer.fabric.org/orderers/orderer3.fabric.org/tls/server.crt
cp $PWD/organizations/ordererOrganizations/orderer.fabric.org/orderers/orderer3.fabric.org/tls/keystore/* $PWD/organizations/ordererOrganizations/orderer.fabric.org/orderers/orderer3.fabric.org/tls/server.key

mkdir -p $PWD/organizations/ordererOrganizations/orderer.fabric.org/orderers/orderer3.fabric.org/msp/tlscacerts
cp $PWD/organizations/ordererOrganizations/orderer.fabric.org/orderers/orderer3.fabric.org/tls/tlscacerts/* $PWD/organizations/ordererOrganizations/orderer.fabric.org/orderers/orderer3.fabric.org/msp/tlscacerts/tlsca.fabric.org-cert.pem

cp $PWD/organizations/ordererOrganizations/orderer.fabric.org/orderers/orderer3.fabric.org/tls/tlscacerts/* $PWD/organizations/ordererOrganizations/orderer.fabric.org/msp/tlscacerts/tlsca.fabric.org-cert.pem

# -----------------------------------------------------------------------
#  Orderer 4

echo "Preparing Orderer 4"

mkdir -p $PWD/organizations/ordererOrganizations/orderer.fabric.org/orderers/orderer4.fabric.org

echo "Generate the orderer msp"
fabric-ca-client enroll -u https://ordererAdmin:ordererAdminpw@ca.orderer.fabric.org:7054 --caname ca-orderer -M $PWD/organizations/ordererOrganizations/orderer.fabric.org/orderers/orderer4.fabric.org/msp --csr.hosts orderer4.fabric.org --csr.hosts localhost --tls.certfiles $PWD/organizations/fabric-ca/orderer/tls-cert.pem

cp $PWD/organizations/ordererOrganizations/orderer.fabric.org/msp/config.yaml $PWD/organizations/ordererOrganizations/orderer.fabric.org/orderers/orderer4.fabric.org/msp/config.yaml

echo "Generate the orderer-tls certificates"
fabric-ca-client enroll -u https://ordererAdmin:ordererAdminpw@ca.orderer.fabric.org:7054 --caname ca-orderer -M $PWD/organizations/ordererOrganizations/orderer.fabric.org/orderers/orderer4.fabric.org/tls --enrollment.profile tls --csr.hosts orderer4.fabric.org --csr.hosts localhost --tls.certfiles $PWD/organizations/fabric-ca/orderer/tls-cert.pem

cp $PWD/organizations/ordererOrganizations/orderer.fabric.org/orderers/orderer4.fabric.org/tls/tlscacerts/* $PWD/organizations/ordererOrganizations/orderer.fabric.org/orderers/orderer4.fabric.org/tls/ca.crt
cp $PWD/organizations/ordererOrganizations/orderer.fabric.org/orderers/orderer4.fabric.org/tls/signcerts/* $PWD/organizations/ordererOrganizations/orderer.fabric.org/orderers/orderer4.fabric.org/tls/server.crt
cp $PWD/organizations/ordererOrganizations/orderer.fabric.org/orderers/orderer4.fabric.org/tls/keystore/* $PWD/organizations/ordererOrganizations/orderer.fabric.org/orderers/orderer4.fabric.org/tls/server.key

mkdir -p $PWD/organizations/ordererOrganizations/orderer.fabric.org/orderers/orderer4.fabric.org/msp/tlscacerts
cp $PWD/organizations/ordererOrganizations/orderer.fabric.org/orderers/orderer4.fabric.org/tls/tlscacerts/* $PWD/organizations/ordererOrganizations/orderer.fabric.org/orderers/orderer4.fabric.org/msp/tlscacerts/tlsca.fabric.org-cert.pem

cp $PWD/organizations/ordererOrganizations/orderer.fabric.org/orderers/orderer4.fabric.org/tls/tlscacerts/* $PWD/organizations/ordererOrganizations/orderer.fabric.org/msp/tlscacerts/tlsca.fabric.org-cert.pem

# -----------------------------------------------------------------------
#  Orderer 5

echo "Preparing Orderer 5"

mkdir -p $PWD/organizations/ordererOrganizations/orderer.fabric.org/orderers/orderer5.fabric.org

echo "Generate the orderer msp"
fabric-ca-client enroll -u https://ordererAdmin:ordererAdminpw@ca.orderer.fabric.org:7054 --caname ca-orderer -M $PWD/organizations/ordererOrganizations/orderer.fabric.org/orderers/orderer5.fabric.org/msp --csr.hosts orderer5.fabric.org --csr.hosts localhost --tls.certfiles $PWD/organizations/fabric-ca/orderer/tls-cert.pem

cp $PWD/organizations/ordererOrganizations/orderer.fabric.org/msp/config.yaml $PWD/organizations/ordererOrganizations/orderer.fabric.org/orderers/orderer5.fabric.org/msp/config.yaml

echo "Generate the orderer-tls certificates"
fabric-ca-client enroll -u https://ordererAdmin:ordererAdminpw@ca.orderer.fabric.org:7054 --caname ca-orderer -M $PWD/organizations/ordererOrganizations/orderer.fabric.org/orderers/orderer5.fabric.org/tls --enrollment.profile tls --csr.hosts orderer5.fabric.org --csr.hosts localhost --tls.certfiles $PWD/organizations/fabric-ca/orderer/tls-cert.pem

cp $PWD/organizations/ordererOrganizations/orderer.fabric.org/orderers/orderer5.fabric.org/tls/tlscacerts/* $PWD/organizations/ordererOrganizations/orderer.fabric.org/orderers/orderer5.fabric.org/tls/ca.crt
cp $PWD/organizations/ordererOrganizations/orderer.fabric.org/orderers/orderer5.fabric.org/tls/signcerts/* $PWD/organizations/ordererOrganizations/orderer.fabric.org/orderers/orderer5.fabric.org/tls/server.crt
cp $PWD/organizations/ordererOrganizations/orderer.fabric.org/orderers/orderer5.fabric.org/tls/keystore/* $PWD/organizations/ordererOrganizations/orderer.fabric.org/orderers/orderer5.fabric.org/tls/server.key

mkdir -p $PWD/organizations/ordererOrganizations/orderer.fabric.org/orderers/orderer5.fabric.org/msp/tlscacerts
cp $PWD/organizations/ordererOrganizations/orderer.fabric.org/orderers/orderer5.fabric.org/tls/tlscacerts/* $PWD/organizations/ordererOrganizations/orderer.fabric.org/orderers/orderer5.fabric.org/msp/tlscacerts/tlsca.fabric.org-cert.pem

cp $PWD/organizations/ordererOrganizations/orderer.fabric.org/orderers/orderer5.fabric.org/tls/tlscacerts/* $PWD/organizations/ordererOrganizations/orderer.fabric.org/msp/tlscacerts/tlsca.fabric.org-cert.pem
