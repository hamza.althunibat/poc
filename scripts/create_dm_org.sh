#!/bin/sh

echo "Configuring DM Org CA Settings"
echo "Enroll the CA admin"
sleep 2
mkdir -p $PWD/organizations/peerOrganizations/dm.fabric.org/

fabric-ca-client enroll -u https://admin:adminpw@ca.dm.fabric.org:7054 --caname ca-dm --tls.certfiles $PWD/organizations/fabric-ca/dm/tls-cert.pem

echo 'NodeOUs:
    Enable: true
    ClientOUIdentifier:
        Certificate: cacerts/ca-dm-fabric-org-7054-ca-dm.pem
        OrganizationalUnitIdentifier: client
    PeerOUIdentifier:
        Certificate: cacerts/ca-dm-fabric-org-7054-ca-dm.pem
        OrganizationalUnitIdentifier: peer
    AdminOUIdentifier:
        Certificate: cacerts/ca-dm-fabric-org-7054-ca-dm.pem
        OrganizationalUnitIdentifier: admin
    OrdererOUIdentifier:
        Certificate: cacerts/ca-dm-fabric-org-7054-ca-dm.pem
        OrganizationalUnitIdentifier: orderer' >$PWD/organizations/peerOrganizations/dm.fabric.org/msp/config.yaml

echo "Register peer0"
fabric-ca-client register --caname ca-dm --id.name peer0 --id.secret peer0pw --id.type peer --tls.certfiles $PWD/organizations/fabric-ca/dm/tls-cert.pem

echo "Register user"
fabric-ca-client register --caname ca-dm --id.name user1 --id.secret user1pw --id.type client --tls.certfiles $PWD/organizations/fabric-ca/dm/tls-cert.pem

echo "Register the org admin"
fabric-ca-client register --caname ca-dm --id.name dmAdmin --id.secret dmAdminpw --id.type admin --tls.certfiles $PWD/organizations/fabric-ca/dm/tls-cert.pem

mkdir -p $PWD/organizations/peerOrganizations/dm.fabric.org/peers

echo "Generate the peer0 msp"
mkdir -p $PWD/organizations/peerOrganizations/dm.fabric.org/peers/peer0.dm.fabric.org
fabric-ca-client enroll -u https://peer0:peer0pw@ca.dm.fabric.org:7054 --caname ca-dm -M $PWD/organizations/peerOrganizations/dm.fabric.org/peers/peer0.dm.fabric.org/msp --csr.hosts peer0.dm.fabric.org --tls.certfiles $PWD/organizations/fabric-ca/dm/tls-cert.pem

cp $PWD/organizations/peerOrganizations/dm.fabric.org/msp/config.yaml $PWD/organizations/peerOrganizations/dm.fabric.org/peers/peer0.dm.fabric.org/msp/config.yaml

echo "Generate the peer0-tls certificates"
fabric-ca-client enroll -u https://peer0:peer0pw@ca.dm.fabric.org:7054 --caname ca-dm -M $PWD/organizations/peerOrganizations/dm.fabric.org/peers/peer0.dm.fabric.org/tls --enrollment.profile tls --csr.hosts peer0.dm.fabric.org --csr.hosts localhost --tls.certfiles $PWD/organizations/fabric-ca/dm/tls-cert.pem

cp $PWD/organizations/peerOrganizations/dm.fabric.org/peers/peer0.dm.fabric.org/tls/tlscacerts/* $PWD/organizations/peerOrganizations/dm.fabric.org/peers/peer0.dm.fabric.org/tls/ca.crt
cp $PWD/organizations/peerOrganizations/dm.fabric.org/peers/peer0.dm.fabric.org/tls/signcerts/* $PWD/organizations/peerOrganizations/dm.fabric.org/peers/peer0.dm.fabric.org/tls/server.crt
cp $PWD/organizations/peerOrganizations/dm.fabric.org/peers/peer0.dm.fabric.org/tls/keystore/* $PWD/organizations/peerOrganizations/dm.fabric.org/peers/peer0.dm.fabric.org/tls/server.key

mkdir -p $PWD/organizations/peerOrganizations/dm.fabric.org/msp/tlscacerts
cp $PWD/organizations/peerOrganizations/dm.fabric.org/peers/peer0.dm.fabric.org/tls/tlscacerts/* $PWD/organizations/peerOrganizations/dm.fabric.org/msp/tlscacerts/ca.crt

mkdir -p $PWD/organizations/peerOrganizations/dm.fabric.org/tlsca
cp $PWD/organizations/peerOrganizations/dm.fabric.org/peers/peer0.dm.fabric.org/tls/tlscacerts/* $PWD/organizations/peerOrganizations/dm.fabric.org/tlsca/tlsca.dm.fabric.org-cert.pem

mkdir -p $PWD/organizations/peerOrganizations/dm.fabric.org/ca
cp $PWD/organizations/peerOrganizations/dm.fabric.org/peers/peer0.dm.fabric.org/msp/cacerts/* $PWD/organizations/peerOrganizations/dm.fabric.org/ca/ca.dm.fabric.org-cert.pem

mkdir -p $PWD/organizations/peerOrganizations/dm.fabric.org/users

echo "Generate the user msp"
mkdir -p $PWD/organizations/peerOrganizations/dm.fabric.org/users/User1@dm.fabric.org
fabric-ca-client enroll -u https://user1:user1pw@ca.dm.fabric.org:7054 --caname ca-dm -M $PWD/organizations/peerOrganizations/dm.fabric.org/users/User1@dm.fabric.org/msp --tls.certfiles $PWD/organizations/fabric-ca/dm/tls-cert.pem
cp $PWD/organizations/peerOrganizations/dm.fabric.org/msp/config.yaml $PWD/organizations/peerOrganizations/dm.fabric.org/users/User1@dm.fabric.org/msp/config.yaml

echo "Generate the org admin msp"
mkdir -p $PWD/organizations/peerOrganizations/dm.fabric.org/users/Admin@dm.fabric.org
fabric-ca-client enroll -u https://dmAdmin:dmAdminpw@ca.dm.fabric.org:7054 --caname ca-dm -M $PWD/organizations/peerOrganizations/dm.fabric.org/users/Admin@dm.fabric.org/msp --tls.certfiles $PWD/organizations/fabric-ca/dm/tls-cert.pem
cp $PWD/organizations/peerOrganizations/dm.fabric.org/msp/config.yaml $PWD/organizations/peerOrganizations/dm.fabric.org/users/Admin@dm.fabric.org/msp/config.yaml
